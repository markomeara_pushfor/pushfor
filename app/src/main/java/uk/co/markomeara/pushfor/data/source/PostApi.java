package uk.co.markomeara.pushfor.data.source;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import uk.co.markomeara.pushfor.data.Comment;
import uk.co.markomeara.pushfor.data.Post;

public interface PostApi {

    @GET("posts")
    Observable<Response<List<Post>>> getPosts();

    @GET("posts/{postId}")
    Observable<Response<Post>> getPost(@Path("postId") long postId);

    @GET("posts/{postId}/comments")
    Observable<Response<List<Comment>>> getPostComments(@Path("postId") long postId);
}
