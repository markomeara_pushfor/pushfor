package uk.co.markomeara.pushfor.postlist;

import java.util.List;

import uk.co.markomeara.pushfor.data.Post;

public interface PostListContract {

    interface View {
        void displayPostList(List<Post> postList);
        void showPostDetails(Post post);
        void setPresenter(PostListPresenter presenter);
        void displayRequestError(Throwable error);
        void hideProgressBar();
        void showProgressBar();
    }

    interface Presenter {
        void postClicked(Post post);
        void loadAllPosts();
    }
}
