package uk.co.markomeara.pushfor.postdetails.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.markomeara.pushfor.R;
import uk.co.markomeara.pushfor.data.Comment;

public class CommentsListAdapter extends RecyclerView.Adapter<CommentsListAdapter.CommentViewHolder> {

    private List<Comment> commentList = new ArrayList<>();
    public void updateCommentList(List<Comment> comments) {
        commentList = comments;
        notifyDataSetChanged();
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.postdetails_comment_item, parent, false);
        return new CommentViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        Comment comment = commentList.get(position);
        holder.setCommentNameEmail(comment.getName(), comment.getEmail());
        holder.setCommentBody(comment.getBody());
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public static class CommentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.comment_name_email)
        TextView nameEmailView;
        @BindView(R.id.comment_body)
        TextView bodyView;

        private Context context;

        CommentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.context = itemView.getContext();
        }

        void setCommentNameEmail(String name, String email) {
            String nameEmail = String.format(context.getString(R.string.comment_name_email), name, email);
            nameEmailView.setText(nameEmail);
        }

        void setCommentBody(String body) {
            bodyView.setText(body);
        }
    }
}
