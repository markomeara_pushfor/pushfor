package uk.co.markomeara.pushfor.di;

import javax.inject.Singleton;

import dagger.Component;
import uk.co.markomeara.pushfor.postdetails.PostDetailsActivity;
import uk.co.markomeara.pushfor.postlist.PostListActivity;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(PostListActivity activity);
    void inject(PostDetailsActivity activity);
}