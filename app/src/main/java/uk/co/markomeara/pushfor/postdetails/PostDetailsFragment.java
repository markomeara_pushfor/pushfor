package uk.co.markomeara.pushfor.postdetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import uk.co.markomeara.pushfor.R;
import uk.co.markomeara.pushfor.data.Comment;
import uk.co.markomeara.pushfor.postdetails.adapter.CommentsListAdapter;

public class PostDetailsFragment extends Fragment implements PostDetailsContract.View {

    private static final String TAG = PostDetailsFragment.class.getSimpleName();

    @BindView(R.id.post_details_title)
    TextView postTitle;

    @BindView(R.id.post_details_body)
    TextView postBody;

    @BindView(R.id.post_details_comments_list)
    RecyclerView postListView;

    private Unbinder viewUnbinder;
    private CommentsListAdapter listAdapter = new CommentsListAdapter();
    private PostDetailsContract.Presenter presenter;

    public static PostDetailsFragment newInstance() {
        return new PostDetailsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.postdetails_fragment, container, false);
        viewUnbinder = ButterKnife.bind(this, view);
        setUpList();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (presenter != null) {
            presenter.loadPostDetails();
        }
    }

    private void setUpList() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        postListView.setLayoutManager(layoutManager);
        postListView.setAdapter(listAdapter);
        postListView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(postListView.getContext(),
                layoutManager.getOrientation());
        postListView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public void setPostTitle(String title) {
        postTitle.setText(title);
    }

    @Override
    public void setPostBody(String body) {
        postBody.setText(body);
    }

    @Override
    public void displayComments(List<Comment> comments) {
        listAdapter.updateCommentList(comments);
    }

    @Override
    public void setPresenter(PostDetailsContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showError(Throwable error) {
        Log.e(TAG, error.getMessage(), error);
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewUnbinder.unbind();
    }
}
