package uk.co.markomeara.pushfor.postdetails;

import java.util.List;

import uk.co.markomeara.pushfor.data.Comment;

public interface PostDetailsContract {

    interface View {
        void setPostTitle(String title);
        void setPostBody(String body);
        void displayComments(List<Comment> comments);
        void setPresenter(PostDetailsContract.Presenter presenter);

        void showError(Throwable error);
    }

    interface Presenter {
        void loadPostDetails();
    }
}
