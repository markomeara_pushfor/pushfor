package uk.co.markomeara.pushfor.postlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import uk.co.markomeara.pushfor.R;
import uk.co.markomeara.pushfor.data.Post;
import uk.co.markomeara.pushfor.postdetails.PostDetailsActivity;
import uk.co.markomeara.pushfor.postlist.adapter.PostListAdapter;

public class PostListFragment extends Fragment implements PostListContract.View {

    private static final String TAG = PostListFragment.class.getSimpleName();

    @BindView(R.id.post_list)
    RecyclerView postListView;

    @BindView(R.id.list_progress_bar)
    ProgressBar progressBar;

    private Unbinder viewUnbinder;
    private PostListContract.Presenter presenter;
    private PostListAdapter listAdapter = new PostListAdapter();

    public static PostListFragment newInstance() {
        return new PostListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.postlist_fragment, container, false);
        viewUnbinder = ButterKnife.bind(this, view);
        setUpList();
        return view;
    }

    private void setUpList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        postListView.setLayoutManager(layoutManager);
        postListView.setAdapter(listAdapter);
        postListView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(postListView.getContext(),
                layoutManager.getOrientation());
        postListView.addItemDecoration(dividerItemDecoration);

        listAdapter.getPostClickObservable()
                .subscribe(postClicked -> presenter.postClicked(postClicked));
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.loadAllPosts();
    }

    @Override
    public void displayPostList(List<Post> postList) {
        listAdapter.updatePostList(postList);
    }

    @Override
    public void showPostDetails(Post post) {
        Intent showPostIntent = new Intent(getActivity(), PostDetailsActivity.class);
        showPostIntent.putExtra(PostDetailsActivity.POST_ID_EXTRA, post.getId());
        getActivity().startActivity(showPostIntent);
    }

    @Override
    public void setPresenter(PostListPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void displayRequestError(Throwable throwable) {
        Log.e(TAG, throwable.getMessage(), throwable);
        Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewUnbinder.unbind();
    }
}
