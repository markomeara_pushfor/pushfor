package uk.co.markomeara.pushfor.postdetails;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Observable;
import uk.co.markomeara.pushfor.data.Comment;
import uk.co.markomeara.pushfor.data.Post;
import uk.co.markomeara.pushfor.data.source.PostService;
import uk.co.markomeara.pushfor.testrule.RxSchedulersTestRule;
import uk.co.markomeara.pushfor.util.ApiContentHelper;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PostDetailsPresenterTest {

    private static final long POST_ID = 1;

    @Rule
    public RxSchedulersTestRule rxSchedulersTestRule = new RxSchedulersTestRule();

    @Mock
    PostDetailsContract.View postDetailsView;

    @Mock
    PostService postService;

    private PostDetailsPresenter presenter;
    private Post post;
    private List<Comment> commentList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new PostDetailsPresenter(postService, postDetailsView, POST_ID);
        post = ApiContentHelper.generateSimplePost();
        commentList = ApiContentHelper.generateSimpleCommentList();
    }

    @Test
    public void testPostSentToView() {
        when(postService.getPost(eq(POST_ID))).thenReturn(Observable.just(post));
        when(postService.getCommentsForPost(eq(POST_ID))).thenReturn(Observable.just(commentList));

        presenter.loadPostDetails();

        verify(postDetailsView).setPostBody(eq(post.getBody()));
        verify(postDetailsView).setPostTitle(eq(post.getTitle()));
    }

    @Test
    public void testCommentListSentToView() {
        when(postService.getPost(eq(POST_ID))).thenReturn(Observable.just(post));
        when(postService.getCommentsForPost(eq(POST_ID))).thenReturn(Observable.just(commentList));

        presenter.loadPostDetails();

        verify(postDetailsView).displayComments(eq(commentList));
    }

    @Test
    public void testErrorShownWhenErrorRetrievingPost() {
        Exception exception = new Exception();
        when(postService.getPost(eq(POST_ID))).thenReturn(Observable.error(exception));
        when(postService.getCommentsForPost(eq(POST_ID))).thenReturn(Observable.just(commentList));

        presenter.loadPostDetails();

        verify(postDetailsView).showError(eq(exception));
    }

    @Test
    public void testErrorShownWhenErrorRetrievingComments() {
        Exception exception = new Exception();
        when(postService.getPost(eq(POST_ID))).thenReturn(Observable.just(post));
        when(postService.getCommentsForPost(eq(POST_ID))).thenReturn(Observable.error(exception));

        presenter.loadPostDetails();

        verify(postDetailsView).showError(eq(exception));
    }

}
