package uk.co.markomeara.pushfor.postlist;

import uk.co.markomeara.pushfor.data.Post;
import uk.co.markomeara.pushfor.data.source.PostService;

import static dagger.internal.Preconditions.checkNotNull;

public class PostListPresenter implements PostListContract.Presenter {

    private PostListContract.View view;
    private PostService postService;

    public PostListPresenter(PostListContract.View contactListView, PostService postService) {
        this.view = checkNotNull(contactListView, "ContactListView cannot be null");
        this.postService = checkNotNull(postService, "PostService cannot be null");
        view.setPresenter(this);
    }

    @Override
    public void postClicked(Post post) {
        view.showPostDetails(post);
    }

    @Override
    public void loadAllPosts() {
        view.showProgressBar();

        postService.getPosts()
                .subscribe(
                        postList -> view.displayPostList(postList),
                        error -> {
                            view.displayRequestError(error);
                            view.hideProgressBar();
                        },
                        () -> view.hideProgressBar()
                );
    }
}
