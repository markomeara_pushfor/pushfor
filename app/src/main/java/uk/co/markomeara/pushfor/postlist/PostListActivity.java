package uk.co.markomeara.pushfor.postlist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import uk.co.markomeara.pushfor.R;
import uk.co.markomeara.pushfor.data.source.PostService;
import uk.co.markomeara.pushfor.di.Injector;
import uk.co.markomeara.pushfor.util.ActivityUtils;

public class PostListActivity extends AppCompatActivity {

    @Inject
    PostService postService;

    private PostListContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.postlist_activity);
        Injector.get().inject(this);

        PostListFragment postListFragment = (PostListFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (postListFragment == null) {
            // Create the fragment
            postListFragment = PostListFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), postListFragment, R.id.contentFrame);
        }
        presenter = new PostListPresenter(postListFragment, postService);
    }
}