package uk.co.markomeara.pushfor;

import android.support.multidex.MultiDexApplication;
import uk.co.markomeara.pushfor.di.Injector;

public class Application extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.init();
    }
}