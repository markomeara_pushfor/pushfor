package uk.co.markomeara.pushfor.service;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Response;
import uk.co.markomeara.pushfor.data.Comment;
import uk.co.markomeara.pushfor.data.Post;
import uk.co.markomeara.pushfor.data.source.PostApi;
import uk.co.markomeara.pushfor.data.source.PostService;
import uk.co.markomeara.pushfor.testrule.RxSchedulersTestRule;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PostServiceTest {

    @Rule
    public RxSchedulersTestRule rxSchedulersTestRule = new RxSchedulersTestRule();

    @Mock
    PostApi postApi;

    @Mock
    Observer subscriber;

    private PostService postService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        postService = new PostService(postApi);
    }

    @Test
    public void testExceptionOccursWhenServerErrorRetrievingPost() {
        Response<Post> errorResponse = Response.error(501, emptyResponseBody());
        Observable<Response<Post>> observableErrorResponse = Observable.just(errorResponse);
        when(postApi.getPost(anyLong())).thenReturn(observableErrorResponse);

        postService.getPost(1).subscribe(subscriber);

        verify(subscriber).onError(any());
    }

    @Test
    public void testExceptionOccursWhenServerErrorRetrievingPostList() {
        Response<List<Post>> errorResponse = Response.error(501, emptyResponseBody());
        Observable<Response<List<Post>>> observableErrorResponse = Observable.just(errorResponse);
        when(postApi.getPosts()).thenReturn(observableErrorResponse);

        postService.getPosts().subscribe(subscriber);

        verify(subscriber).onError(any());
    }

    @Test
    public void testExceptionOccursWhenServerErrorRetrievingCommentList() {
        Response<List<Comment>> errorResponse = Response.error(501, emptyResponseBody());
        Observable<Response<List<Comment>>> observableErrorResponse = Observable.just(errorResponse);
        when(postApi.getPostComments(1)).thenReturn(observableErrorResponse);

        postService.getPosts().subscribe(subscriber);

        verify(subscriber).onError(any());
    }

    private static ResponseBody emptyResponseBody() {
        return new ResponseBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public long contentLength() {
                return 0;
            }

            @Override
            public BufferedSource source() {
                return null;
            }
        };
    }


}
