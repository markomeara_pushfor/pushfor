package uk.co.markomeara.pushfor.postlist.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import uk.co.markomeara.pushfor.R;
import uk.co.markomeara.pushfor.data.Post;

public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.PostViewHolder> {

    private final PublishSubject<Post> onPostClickSubject = PublishSubject.create();
    private List<Post> postList = new ArrayList<>();

    public void updatePostList(List<Post> posts) {
        postList = posts;
        notifyDataSetChanged();
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.postlist_item, parent, false);
        return new PostViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        holder.setPostTitle(postList.get(position).getTitle());
        holder.setPostBody(postList.get(position).getBody());

        holder.setOnClickListener(view -> onPostClickSubject.onNext(postList.get(position)));
    }

    public Observable<Post> getPostClickObservable() {
        return onPostClickSubject;
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public static class PostViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.post_title)
        TextView titleView;
        @BindView(R.id.post_body)
        TextView bodyView;

        private View itemView;

        PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemView = itemView;
        }

        void setPostTitle(String title) {
            titleView.setText(title);
        }

        void setPostBody(String body) {
            bodyView.setText(body);
        }

        void setOnClickListener(View.OnClickListener listener) {
            itemView.setOnClickListener(listener);
        }
    }
}
