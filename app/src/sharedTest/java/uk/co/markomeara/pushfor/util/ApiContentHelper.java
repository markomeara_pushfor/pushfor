package uk.co.markomeara.pushfor.util;

import java.util.ArrayList;
import java.util.List;

import uk.co.markomeara.pushfor.data.Comment;
import uk.co.markomeara.pushfor.data.Post;

public class ApiContentHelper {

    private ApiContentHelper() {
        throw new AssertionError();
    }

    public static Post generateSimplePost() {
        Post post1 = new Post();
        post1.setTitle("My first post title");
        post1.setBody("My first post body");
        post1.setUserId(123);
        post1.setId(1);

        return post1;
    }

    public static List<Post> generateSimplePostList() {
        Post post1 = new Post();
        post1.setTitle("My first post title");
        post1.setBody("My first post body");
        post1.setUserId(123);
        post1.setId(1);

        Post post2 = new Post();
        post2.setTitle("My second post title");
        post2.setBody("My second post body");
        post2.setUserId(456);
        post2.setId(2);

        List<Post> postList = new ArrayList<>();
        postList.add(post1);
        postList.add(post2);

        return postList;
    }

    public static List<Comment> generateSimpleCommentList() {
        Comment comment1 = new Comment();
        comment1.setBody("My comment body");
        comment1.setEmail("myemail@example.com");
        comment1.setName("Mark O'Meara");

        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment1);
        return commentList;
    }
}
