package uk.co.markomeara.pushfor.postdetails;

import uk.co.markomeara.pushfor.data.source.PostService;

import static dagger.internal.Preconditions.checkNotNull;

public class PostDetailsPresenter implements PostDetailsContract.Presenter {

    private PostDetailsContract.View view;
    private PostService postService;
    private long postId;

    public PostDetailsPresenter(PostService postService, PostDetailsContract.View view, long postId) {
        this.postService = checkNotNull(postService, "postService cannot be null");
        this.view = checkNotNull(view, "view cannot be null");
        this.postId = checkNotNull(postId, "postId cannot be null");
        view.setPresenter(this);
    }

    @Override
    public void loadPostDetails() {
        loadPostContent(); // Re-fetching post as content may have been updated
        loadPostComments();
    }

    private void loadPostContent() {
        postService.getPost(postId)
                .subscribe(
                        post -> {
                            view.setPostTitle(post.getTitle());
                            view.setPostBody(post.getBody());
                        },
                        error -> view.showError(error)
                );
    }

    private void loadPostComments() {
        postService.getCommentsForPost(postId)
                .subscribe(
                        comments -> view.displayComments(comments),
                        error -> view.showError(error)
                );
    }
}
