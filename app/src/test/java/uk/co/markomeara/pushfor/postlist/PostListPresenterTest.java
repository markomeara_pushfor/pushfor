package uk.co.markomeara.pushfor.postlist;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Observable;
import uk.co.markomeara.pushfor.data.Post;
import uk.co.markomeara.pushfor.data.source.PostService;
import uk.co.markomeara.pushfor.testrule.RxSchedulersTestRule;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static uk.co.markomeara.pushfor.util.ApiContentHelper.generateSimplePostList;

public class PostListPresenterTest {

    @Rule
    public RxSchedulersTestRule rxSchedulersTestRule = new RxSchedulersTestRule();

    @Mock
    PostListContract.View postListView;

    @Mock
    PostService postService;

    private PostListPresenter presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new PostListPresenter(postListView, postService);
    }

    @Test
    public void testPostListSentToViewWhenReceived() {
        List<Post> postListReturned = generateSimplePostList();
        Observable<List<Post>> postListObservable = Observable.just(postListReturned);

        when(postService.getPosts()).thenReturn(postListObservable);

        presenter.loadAllPosts();

        verify(postListView).displayPostList(eq(postListReturned));
    }

    @Test
    public void testProgressBarShownWhenLoadingPosts() {
        when(postService.getPosts()).thenReturn(Observable.empty());

        presenter.loadAllPosts();

        verify(postListView).showProgressBar();
    }

    @Test
    public void testProgressBarHiddenAfterRequestError() {
        when(postService.getPosts()).thenReturn(Observable.error(new Exception()));

        presenter.loadAllPosts();

        verify(postListView).hideProgressBar();
    }


    @Test
    public void testErrorShownAfterRequestError() {
        Exception exception = new Exception();
        when(postService.getPosts()).thenReturn(Observable.error(exception));

        presenter.loadAllPosts();

        verify(postListView).displayRequestError(eq(exception));
    }

    @Test
    public void testProgressBarHiddenAfterSuccessfulRequest() {
        List<Post> postList = generateSimplePostList();

        when(postService.getPosts()).thenReturn(Observable.just(postList));

        presenter.loadAllPosts();

        verify(postListView).hideProgressBar();
    }

}
