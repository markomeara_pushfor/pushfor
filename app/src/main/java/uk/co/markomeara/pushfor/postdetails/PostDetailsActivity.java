package uk.co.markomeara.pushfor.postdetails;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import uk.co.markomeara.pushfor.R;
import uk.co.markomeara.pushfor.data.source.PostService;
import uk.co.markomeara.pushfor.di.Injector;
import uk.co.markomeara.pushfor.util.ActivityUtils;

public class PostDetailsActivity extends AppCompatActivity {

    public static final String POST_ID_EXTRA = "post_id";

    @Inject
    PostService postService;

    private PostDetailsContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.postlist_activity);
        Injector.get().inject(this);

        long postId = getIntent().getLongExtra(POST_ID_EXTRA, -1L);

        PostDetailsFragment postDetailsFragment = (PostDetailsFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (postDetailsFragment == null) {
            // Create the fragment
            postDetailsFragment = PostDetailsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), postDetailsFragment, R.id.contentFrame);
        }
        presenter = new PostDetailsPresenter(postService, postDetailsFragment, postId);
    }
}
