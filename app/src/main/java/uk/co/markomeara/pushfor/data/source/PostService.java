package uk.co.markomeara.pushfor.data.source;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import uk.co.markomeara.pushfor.data.Comment;
import uk.co.markomeara.pushfor.data.Post;

public class PostService {

    private PostApi postApi;

    public PostService(PostApi api) {
        this.postApi = api;
    }

    public Observable<List<Post>> getPosts() {
        return Observable.defer(() -> postApi.getPosts())
                .subscribeOn(Schedulers.io())
                .doOnNext(this::errorCheck)
                .map(Response::body)
                .observeOn(AndroidSchedulers.mainThread());

    }

    public Observable<Post> getPost(long postId) {
        return Observable.defer(() -> postApi.getPost(postId))
                .subscribeOn(Schedulers.io())
                .doOnNext(this::errorCheck)
                .map(Response::body)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Comment>> getCommentsForPost(long postId) {
        return Observable.defer(() -> postApi.getPostComments(postId))
                .subscribeOn(Schedulers.io())
                .doOnNext(this::errorCheck)
                .map(Response::body)
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void errorCheck(Response response) {
        if (!response.isSuccessful()) {
            Exceptions.propagate(new Exception("Server code " + response.code() + " received"));
        }
    }
}
